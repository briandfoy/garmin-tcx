package Garmin::TCX;
use strict;

use warnings;
no warnings;

use subs qw();
use vars qw($VERSION);

$VERSION = '0.10_01';

=encoding utf8

=head1 NAME

Garmin::TCX - Interact with TCX sports data files

=head1 SYNOPSIS

	use Garmin::TCX;

	my $tcx = Garmin::TCX->from_file( $file );
	my $tcx = Garmin::TCX->from_filehandle( $fh );
	
	my @folders    = $tcx->folders;
	my @activities = $tcx->activities;
	my @workouts   = $tcx->workouts;
	my @courses    = $tcx->courses;
	my @notes      = $tcx->notes;
	
	
=head1 DESCRIPTION

=over 4

=item new_from_file

=cut

sub new_from_file {
	my( $class, $filename ) = @_;

	open my $fh, '<:utf8', $filename;
	
	$class->new_from_file( $fh );
	}
	
=item new_from_filehandle

=cut

sub new_from_filehandle
	{
	my( $class, $fh ) = @_;
	
	my $self = bless {
		_twig      => $twig,
		Activities => [],
		Folders    => [],
		Workouts   => [],
		Courses    => [],
		Notes      => [],
		}, $class;

	my $twig = XML::Twig->new( 
		twig_handlers => {
			Activities => sub { $self->_Activities( $_ ) },
			Folders    => sub { $self->_Folders( $_ )    },
			Workouts   => sub { $self->_Workouts( $_ )   },
			Courses    => sub { $self->_Courses( $_ )    },
			Notes      => sub { $self->_Notes( $_ )      },
			},
		);
		
	}

sub _generic {
	my( $self, $generic_twig ) = @_;
	my $xml_tag = ( caller(1) )[3];
	$xml_tag =~ s/\A_+//;

	my @generics = $generic_twig->children( $xml_tag );
	my $class = "Garmin::TCX::$xml_tag";

	push @{ $self->{$instance_key} }, 
		map { $class->new_from_twig( $_ ) }
		@generics;
	}



sub _Activities { $_[0]->_generic( $_[1] ) }

sub _Folders    { return; $_[0]->_generic( $_[1] ) }

sub _Workouts   { return; $_[0]->_generic( $_[1] ) }

sub _Courses    { return; $_[0]->_generic( $_[1] ) }

sub _Notes      { return; $_[0]->_generic( $_[1] ) }

=item activities

Returns the list of activities as an array reference. Each item is an
object of type C<Garmin::TCX::Activity>.

=item folders

Return the list of folders as an array reference. Each item is an
object of type C<Garmin::TCX::Folder>.

=item workouts

Return the list of folders as an array reference. Each item is an
object of type C<Garmin::TCX::Folder>.

=item courses

Return the list of folders as an array reference. Each item is an
object of type C<Garmin::TCX::Course>.

=item notes

Return the list of folders as an array reference. Each item is an
object of type C<Garmin::TCX::Note>.

=cut

sub activities { $_[0]->{activities} }
sub folders    { $_[0]->{folders} }
sub workouts   { $_[0]->{workouts} }
sub courses    { $_[0]->{courses} }
sub notes      { $_[0]->{notes} }

=back

=head1 TO DO


=head1 SEE ALSO


=head1 SOURCE AVAILABILITY

This source is in Github:

	http://github.com/briandfoy/garmin-tcx/

=head1 AUTHOR

, C<< <bdfoy@cpan.org> >>

=head1 COPYRIGHT AND LICENSE


Copyright © 2012-2015, brian d foy <bdfoy@cpan.org>. All rights reserved.

You may redistribute this under the same terms as Perl itself.

=cut

1;
