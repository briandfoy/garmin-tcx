package Garmin::TCX::Activity;
use strict;

use warnings;
no warnings;

use subs qw();
use vars qw($VERSION);

$VERSION = '0.10_01';

=encoding utf8

=head1 NAME

Garmin::TCX - Interact with TCX sports data files

=head1 SYNOPSIS

	use Garmin::TCX;

	my $tcx = Garmin::TCX->from_file( $file );
	my $tcx = Garmin::TCX->from_filehandle( $fh );
	
	my @activities = $tcx->activities;
	my @workouts   = $tcx->workouts;
	my @courses    = $tcx->courses;


	my $activity   = $activities[0];
	
	
=head1 DESCRIPTION

=over 4

=item new

=cut

sub new_from_twig {
	my( $class, $twig ) = @_;
	
	my $sport = $twig->att('Sport');
	my( $id ) = $twig->child_text( 0 );
	
	my @laps = $twig->children( 'Lap' );
	
	bless {
		Sport => $sport,
		ID    => $id,
		}, $class;
	}
	
=item init

=cut

sub init
	{
	
	}

=back

=head1 TO DO


=head1 SEE ALSO


=head1 SOURCE AVAILABILITY

This source is in Github:

	http://github.com/briandfoy/garmin-tcx/

=head1 AUTHOR

, C<< <bdfoy@cpan.org> >>

=head1 COPYRIGHT AND LICENSE


Copyright © 2012-2015, brian d foy <bdfoy@cpan.org>. All rights reserved.

You may redistribute this under the same terms as Perl itself.

=cut

1;
